# Gitversioninfo

This LabVIEW library allows for retrieving git version information from a
LabVIEW project repository. This version information can be integrated in the
application (e.g. for display in an about dialog etc.)

### Prerequisites

It is assumed that your LabVIEW project is already a git repository. The
Gitversioninfo library needs to be added as git submodule.

### Installing
Add gitversioninfo as submodule to your existing project repository
For ssh connectione (GSI internal only):
```
git submodule add git@git.gsi.de:u.eisenbarth/gitversioninfo.git 
```
Or for (public) HTTPS connection:
```
git submodule add https://git.gsi.de/phelix/lv/gitversioninfo.git 
```
Then initialize and update:
```
git submodule init && git submodule update
```
Open your LabVIEW application project and add the Gitversioninfo library to your
project. Also add the file VersionString.vi to your project. After that add 
the VI "Pre-Build Action.vi" to the application build dialog (Category: Pre/Post Build Actions).

## Using Gitversioninfo

Prior to the build process the Pre-Build Action.vi retrieves the git version
information of the apllication repository and all submodules. This version
information is written to the file VersionSTring.vi, which is simnply a string
control. The output terminal of this VI can then be used e.g. for displaying
version information in your application.

## Remarks

Due to bugs in LabVIEW, the first run of a build process sometimes fails with
an error message (file has been changed on disk). In this case, try to repeat the build.

## Authors

* **Udo Eisenbarth** - *Initial work*
* **Christian Brabetz** - *Initial work*

## Acknowledgments

* Holger Brandt for valuable tips and discussions.
